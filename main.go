package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/go-redis/redis"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
)

type ContractDB struct {
	ContractName    string
	DailyStampsUsed int `json:",omitempty"`
	DailyTxn        int `json:",omitempty"`
	DailyAddresses  int `json:",omitempty"`
	TotalStampsUsed int `json:",omitempty"`
	TotalTxn        int `json:",omitempty"`
	TotalAddresses  int `json:",omitempty"`
}

var syncRunning int = 0

var db *sql.DB
var redisClient *redis.Client

func NewNullString(s string) sql.NullString {
	if len(s) == 0 {
		return sql.NullString{}
	}
	return sql.NullString{
		String: s,
		Valid:  true,
	}
}

//func updateRedis() {
func updateRedis(t time.Time) {
	fmt.Printf("updateRedis\n")
	syncRunning = 1

	sqlStatement := `SELECT contract, COUNT(*) AS txn, SUM(stamps_used) AS stamps_used,
	get_total_address(address_balance) AS total_unique_address from transactions WHERE contract IN('con_databased', 'con_dice001', 'con_gamma_phi_house_v2', 'con_gold_flip', 'con_gold_ticket_003', 'con_hopium', 'con_lamden_link_v1', 'con_mob_dice002', 'con_multipletransfert_contract', 'con_multisend2', 'con_nebula', 'con_optic_protocol', 'con_pixel_whale_master_v1', 'con_pixelcity_master_1', 'con_puzzles_v1', 'con_random_allocator', 'con_reflecttau_v2', 'con_rocketswap_official_v1_1', 'con_simple_staking_tau_rswp_001', 'con_smack_that_6', 'con_spange_game_v1', 'con_staking_rswp_rswp', 'con_uw_master__s__1', 'submission') GROUP BY contract ORDER BY total_unique_address DESC, contract`
	rows, err := db.Query(sqlStatement)
	if err != nil {
		fmt.Printf("line 48\n")
		log.Fatal(err)
	}
	contracts := make(map[string]*ContractDB)
	specialContracts := []string{"con_dice001", "con_gold_flip", "con_gold_ticket_003", "con_multisend2", "con_rocketswap_official_v1_1", "con_simple_staking_tau_rswp_001", "con_staking_rswp_rswp"}
	for _, contractName := range specialContracts {
		contracts[contractName] = &ContractDB{
			DailyTxn:        0,
			DailyStampsUsed: 0,
			DailyAddresses:  0,
			TotalTxn:        0,
			TotalStampsUsed: 0,
			TotalAddresses:  0,
		}
	}

	for rows.Next() {
		var name string
		var stampsUsed int
		var totalUniqeAddress int
		var txn int
		err = rows.Scan(&name, &txn, &stampsUsed, &totalUniqeAddress)
		fmt.Printf("%s: Total StampsUsed: %d Total Txn: %d Total Users: %d\n", name, stampsUsed, txn, totalUniqeAddress)
		err = redisClient.Do("HMSET", name, "total_stamps_used", stampsUsed, "total_txn", txn, "total_addresses", totalUniqeAddress, "daily_stamps_used", 0, "daily_txn", 0, "daily_addresses", 0).Err()
		if err != nil {
			fmt.Printf("line 73\n")
			fmt.Println(err)
		}
		if name == "con_dice001" || name == "con_gold_flip" || name == "con_gold_ticket_003" || name == "con_multisend2" || name == "con_rocketswap_official_v1_1" || name == "con_simple_staking_tau_rswp_001" || name == "con_staking_rswp_rswp" {
			contracts[name].TotalTxn = txn
			contracts[name].TotalStampsUsed = stampsUsed
			contracts[name].TotalAddresses = totalUniqeAddress
		}
	}

	sqlStatement = `SELECT contract, COUNT(*) AS txn, SUM(stamps_used) AS stamps_used,
		get_total_address(address_balance) AS total_unique_address from transactions WHERE contract IN('con_databased', 'con_dice001', 'con_gamma_phi_house_v2', 'con_gold_flip', 'con_gold_ticket_003', 'con_hopium', 'con_lamden_link_v1', 'con_mob_dice002', 'con_multipletransfert_contract', 'con_multisend2', 'con_nebula', 'con_optic_protocol', 'con_pixel_whale_master_v1', 'con_pixelcity_master_1', 'con_puzzles_v1', 'con_random_allocator', 'con_reflecttau_v2', 'con_rocketswap_official_v1_1', 'con_simple_staking_tau_rswp_001', 'con_smack_that_6', 'con_spange_game_v1', 'con_staking_rswp_rswp', 'con_uw_master__s__1', 'submission') AND EXTRACT(EPOCH FROM NOW()) - timestamp < 86400
		GROUP BY contract ORDER BY total_unique_address DESC, contract`
	rows, err = db.Query(sqlStatement)
	if err != nil {
		fmt.Printf("line 88")
		log.Fatal(err)
	}
	for rows.Next() {
		var name string
		var stampsUsed int
		var totalUniqeAddress int
		var txn int
		err = rows.Scan(&name, &txn, &stampsUsed, &totalUniqeAddress)
		fmt.Printf("%s: Daily StampsUsed: %d Daily Txn: %d Daily Users: %d\n", name, stampsUsed, txn, totalUniqeAddress)
		err = redisClient.Do("HMSET", name, "daily_stamps_used", stampsUsed, "daily_txn", txn, "daily_addresses", totalUniqeAddress).Err()
		if err != nil {
			fmt.Printf("line 100\n")
			fmt.Println(err)
		}
		if name == "con_dice001" || name == "con_gold_flip" || name == "con_gold_ticket_003" || name == "con_multisend2" || name == "con_rocketswap_official_v1_1" || name == "con_simple_staking_tau_rswp_001" || name == "con_staking_rswp_rswp" {
			contracts[name].DailyTxn = txn
			contracts[name].DailyStampsUsed = stampsUsed
			contracts[name].DailyAddresses = totalUniqeAddress
		}
	}

	err = redisClient.Do("HMSET", "con_dice001",
		"daily_stamps_used", contracts["con_dice001"].DailyStampsUsed+contracts["con_multisend2"].DailyStampsUsed+contracts["con_gold_flip"].DailyStampsUsed+contracts["con_gold_ticket_003"].DailyStampsUsed,
		"daily_txn", contracts["con_dice001"].DailyTxn+contracts["con_multisend2"].DailyTxn+contracts["con_gold_flip"].DailyTxn+contracts["con_gold_ticket_003"].DailyTxn,
		"daily_addresses", contracts["con_dice001"].DailyAddresses+contracts["con_multisend2"].DailyAddresses+contracts["con_gold_flip"].DailyAddresses+contracts["con_gold_ticket_003"].DailyAddresses,
		"total_stamps_used", contracts["con_dice001"].TotalStampsUsed+contracts["con_multisend2"].TotalStampsUsed+contracts["con_gold_flip"].TotalStampsUsed+contracts["con_gold_ticket_003"].TotalStampsUsed,
		"total_txn", contracts["con_dice001"].TotalTxn+contracts["con_multisend2"].TotalTxn+contracts["con_gold_flip"].TotalTxn+contracts["con_gold_ticket_003"].TotalTxn,
		"total_addresses", contracts["con_dice001"].TotalAddresses+contracts["con_multisend2"].TotalAddresses+contracts["con_gold_flip"].TotalAddresses+contracts["con_gold_ticket_003"].TotalAddresses,
	).Err()
	if err != nil {
		fmt.Printf("line 119\n")
		fmt.Println(err)
	}
	err = redisClient.Do("HMSET", "con_rocketswap_official_v1_1",
		"daily_stamps_used", contracts["con_rocketswap_official_v1_1"].DailyStampsUsed+contracts["con_simple_staking_tau_rswp_001"].DailyStampsUsed+contracts["con_staking_rswp_rswp"].DailyStampsUsed,
		"daily_txn", contracts["con_rocketswap_official_v1_1"].DailyTxn+contracts["con_simple_staking_tau_rswp_001"].DailyTxn+contracts["con_staking_rswp_rswp"].DailyTxn,
		"daily_addresses", contracts["con_rocketswap_official_v1_1"].DailyAddresses+contracts["con_simple_staking_tau_rswp_001"].DailyAddresses+contracts["con_staking_rswp_rswp"].DailyAddresses,
		"total_stamps_used", contracts["con_rocketswap_official_v1_1"].TotalStampsUsed+contracts["con_simple_staking_tau_rswp_001"].TotalStampsUsed+contracts["con_staking_rswp_rswp"].TotalStampsUsed,
		"total_txn", contracts["con_rocketswap_official_v1_1"].TotalTxn+contracts["con_simple_staking_tau_rswp_001"].TotalTxn+contracts["con_staking_rswp_rswp"].TotalTxn,
		"total_addresses", contracts["con_rocketswap_official_v1_1"].TotalAddresses+contracts["con_simple_staking_tau_rswp_001"].TotalAddresses+contracts["con_staking_rswp_rswp"].TotalAddresses,
	).Err()
	if err != nil {
		fmt.Printf("line 131\n")
		fmt.Println(err)
	}

	sqlStatement = `SELECT COUNT(address) AS total FROM top_addresses WHERE first_tx_timestamp != 0`
	row := db.QueryRow(sqlStatement)
	var totalAddresses int
	err = row.Scan(&totalAddresses)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("total Addresses: %d\n", totalAddresses)
	err = redisClient.Set("total_addresses", totalAddresses, 0).Err()
	if err != nil {
		fmt.Println(err)
	}

	sqlStatement = "SELECT COUNT(id) FROM contracts"
	row = db.QueryRow(sqlStatement)
	var totalContracts int
	err = row.Scan(&totalContracts)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("total Contracts: %d\n", totalContracts)
	err = redisClient.Set("total_contracts", totalContracts, 0).Err()
	if err != nil {
		fmt.Println(err)
	}

	sqlStatement = "SELECT COUNT(id) FROM contracts WHERE is_token = '1' AND active = '1'"
	row = db.QueryRow(sqlStatement)
	var totalTokens int
	err = row.Scan(&totalTokens)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("total Tokens: %d\n", totalTokens)
	err = redisClient.Set("total_tokens", totalTokens, 0).Err()
	if err != nil {
		fmt.Println(err)
	}

	sqlStatement = "SELECT COUNT(id) FROM dapps where active = '1'"
	row = db.QueryRow(sqlStatement)
	var totalDapps int
	err = row.Scan(&totalDapps)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("total Dapps: %d\n", totalDapps)
	err = redisClient.Set("total_dapps", totalDapps, 0).Err()
	if err != nil {
		fmt.Println(err)
	}

	sqlStatement = "SELECT FLOOR(SUM(balance)) AS balance FROM top_addresses WHERE address = 'con_burn' OR (first_tx_timestamp != 0 AND LENGTH(TRIM(address)) != 64 AND address NOT LIKE 'con_%' AND balance > 0 AND address NOT IN ('currency_2') OR address = ' 5b09493df6c18d17cc883ebce54fcb1f5afbd507533417fe32c006009a9c3c4a')"
	row = db.QueryRow(sqlStatement)
	var totalTAULost int
	err = row.Scan(&totalTAULost)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("total TAU Lost: %d\n", totalTAULost)
	err = redisClient.Set("total_tau_lost", totalTAULost, 0).Err()
	if err != nil {
		fmt.Println(err)
	}

	syncRunning = 0

}

func doEvery(d time.Duration, f func(time.Time)) {
	for x := range time.Tick(d) {
		fmt.Printf("\n\n\nCalling updateRedis function\n")
		if syncRunning == 0 {
			f(x)
		}

	}
}

func main() {

	fmt.Printf("line main()\n")

	//err := godotenv.Load(".env")
	err := godotenv.Load("/app/config/.env")
	if err != nil {
		log.Fatalf("Error loading .env file")
	}

	redisClient = redis.NewClient(&redis.Options{
		Addr:     os.Getenv("REDISHOST"),
		Password: os.Getenv("REDISPASS"),
		DB:       0,
	})

	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		os.Getenv("DBHOST"), 5000, os.Getenv("DBUSER"), os.Getenv("DBPASS"), os.Getenv("DBNAME"))
	db, err = sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	doEvery(60*time.Second, updateRedis)
	//updateRedis()
}
