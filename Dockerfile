FROM golang:1.16-alpine as builder

WORKDIR /app

COPY . .

RUN go mod download
RUN go build -o /tauhq-top-dapp-arko

EXPOSE 10001


CMD [ "/tauhq-top-dapp-arko" ]

